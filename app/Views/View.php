<?php
namespace App\Views;


class View
{
    /**
     * @var string $title App title
     */
    protected $title;

    /**
     * @var
     */
    protected $template;

    /**
     * View constructor.
     * @param $attributes
     */
    public function __construct($attributes)
    {
        foreach ($attributes as $property => $value) {
            $this->{$property} = $value;
        }
    }

    /**
     * To display this view
     * @param $view
     */
    public function show($view)
    {
        include_once __DIR__ . $view->template;
    }
}
<?php
namespace App\Models;

use App\Configurators\Configurator;

class Model
{
    /**
     * Function is used to build attributes array depending on config and
     * passed parameters
     *
     * @param string $action Template file name
     *
     * @return array Attributes array
     */
    public function getAttributes($action)
    {
        $app = Configurator::app();
        $attributes = [
            'title'    => $app->name,
            'template' => '/' . $app->layout . '/' . $action . '.php',
        ];

        return $attributes;
    }
}
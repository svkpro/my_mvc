<?php
namespace App\Controllers;

use App\Models\Model;
use App\Views\View;

class Controller {

    /**
     * @var Model
     */
    protected $model;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->model = new Model();
    }

    /**
     * The main controller method
     */
    public function index()
    {
        $attributes = $this->model->getAttributes(__FUNCTION__);
        $view = new View($attributes);
        $view->show($view);
    }
}
<?php

namespace Unit\View;

use App\Views\View;
use PHPUnit\Framework\TestCase;

/**
 * Class ViewTest
 * Tests View of the Skeleton
 *
 * @package Unit\View
 */
class ViewTest extends TestCase
{
    const PROPERTY_TITLE_KEY = 'title';
    const PROPERTY_TITLE_VALUE = 'sometitle';
    const PROPERTY_TEMPLATE_KEY = 'template';
    const PROPERTY_TEMPLATE_VALUE = 'sometemplate';

    /**
     * Test class may be instantinated
     *
     * @return void
     */
    public function testCanInstantinate() {
        $view = new View([]);
        $this->assertInstanceOf(View::class, $view);
    }

    /**
     * Test that properties may be set up
     *
     * @return void
     */
    public function testExistentProperties() {
        $view = new View([
            self::PROPERTY_TITLE_KEY => self::PROPERTY_TITLE_VALUE,
            self::PROPERTY_TEMPLATE_KEY => self::PROPERTY_TEMPLATE_VALUE
        ]);
        $this->assertAttributeEquals(
            self::PROPERTY_TEMPLATE_VALUE,
            self::PROPERTY_TEMPLATE_KEY,
            $view
        );
        $this->assertAttributeEquals(
            self::PROPERTY_TITLE_VALUE,
            self::PROPERTY_TITLE_KEY,
            $view
        );
    }

}
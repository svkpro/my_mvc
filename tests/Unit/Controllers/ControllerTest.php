<?php

namespace UnitTests\Controllers;
use App\Controllers\Controller;
use PHPUnit\Framework\TestCase;

/**
 * Class ControllerTest
 * Tests Controller of the skeleton
 *
 * @package UnitTests\Controllers
 */
class ControllerTest extends TestCase
{

    /**
     * Test the Controller may be instantinated
     *
     * @return void
     */
    public function testCanInstantinate() {
        $controller = new Controller();
        $this->assertInstanceOf(Controller::class, $controller);
    }

    /**
     * Test Index action will return a paragraph with text
     *
     * @return void
     */
    public function testIndex() {
        $controller = new Controller();

        ob_start();
        $controller->index();
        $data = ob_get_contents();
        ob_end_clean();

        $expectedDOM = new \DOMDocument();
        $paragraph = $expectedDOM->createElement('p', 'MVC Model');

        $actualDOM = new \DOMDocument();
        $actualDOM->loadXML($data);
        $actualParagraphs = $actualDOM->getElementsByTagName('p');

        $this->assertEquals(1, $actualParagraphs->length);
        $this->assertEqualXMLStructure($paragraph, $actualParagraphs[0]);
    }

}
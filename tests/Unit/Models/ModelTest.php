<?php

namespace UnitTests\Models;

use App\Models\Model;
use PHPUnit\Framework\TestCase;

/**
 * Class ModelTest
 * Tests Model of the skeleton
 *
 * @package UnitTests\Models
 */
class ModelTest extends TestCase {

    /**
     * Test class can be instantinated
     *
     * @return void
     */
    public function testCanInstantinate() {
        $model = new Model();
        $this->assertInstanceOf(Model::class, $model);
    }

    /**
     * Test if GetIndexAttributes returns correct file path and title
     *
     * @dataProvider dpAttributes
     * @param string $attributeName Name of an attribute
     * @param string $filePath      Template file path
     *
     * @return void
     */
    public function testGetIndexAttributes($attributeName, $filePath) {
        $model = new Model();
        $attributes = $model->getAttributes($attributeName);
        $this->assertEquals(
            ['title' => 'MVC Model', 'template' => $filePath],
            $attributes
        );
    }

    /**
     * Data provider for attributes
     *
     * @return array
     */
    public function dpAttributes() {
        return [
            'index' => ['index', '/app/index.php'],
            'some' => ['some', '/app/some.php']
        ];
    }

}
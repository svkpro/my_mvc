<?php

/**
 * Include the autoload
 */
require_once __DIR__. '/vendor/autoload.php';

/**
 * Include the bootstrap
 */
require_once 'app/bootstrap.php';